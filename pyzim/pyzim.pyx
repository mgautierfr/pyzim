from libcpp.string cimport string
from libc.stdint cimport uint32_t, uint64_t

cimport zim_wrapper as zim

cdef class Article:
    cdef zim.Article c_article

    cdef setup(self, zim.Article art):
        self.c_article = art

    def good(self):
        return self.c_article.good()

    def title(self):
        return self.c_article.getTitle()

    def content(self):
        b = self.c_article.getData(<int>0)
        return b.data()[:b.size()]

    def longurl(self):
        return self.c_article.getLongUrl()

    def mimetype(self):
        return self.c_article.getMimeType()

    def is_redirect(self):
        return self.c_article.isRedirect()

    def get_redirect_article(self):
        cdef Article article = Article()
        cdef zim.Article art = self.c_article.getRedirectArticle()
        if not art.good():
            raise RuntimeError("Article is not a redirectArticle")
        article.setup(art)
        return article

cdef class File:
    cdef zim.File c_file

    def __cinit__(self, bytes filename):
        self.c_file = zim.File(filename)

    def get_article(self, bytes url):
        cdef zim.Article art = self.c_file.getArticleByUrl(url)
        cdef Article article = Article()
        if not art.good():
            raise RuntimeError("Article's url is not good")
        article.setup(art)
        return article

    def get_main_page_url(self):
        cdef zim.Fileheader header = self.c_file.getFileheader()
        cdef zim.Article article
        if header.hasMainPage():
            article = self.c_file.getArticle(header.getMainPage())
            return article.getLongUrl();

        # [TODO] Handle old zim where header has no mainPage.
        # (We need to get first article in the zim)
        return ""
