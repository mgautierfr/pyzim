from libcpp.string cimport string
from libc.stdint cimport uint32_t, uint64_t

cdef extern from "zim/zim.h" namespace "zim":
    ctypedef uint32_t size_type
    ctypedef uint64_t offset_type

cdef extern from "zim/blob.h" namespace "zim":
    cdef cppclass Blob:
        char* data()
        char* end()
        int size()

cdef extern from "zim/article.h" namespace "zim":
    cdef cppclass Article:
        Article() except +

        string getTitle()
        string getUrl()
        string getLongUrl()
        string getMimeType()
        bint good()

        const Blob getData(size_type offset)

        bint isRedirect()
        bint isLinktarget()
        bint isDeleted()

        Article getRedirectArticle()


cdef extern from "zim/fileheader.h" namespace "zim":
    cdef cppclass Fileheader:
        bint hasMainPage()
        size_type getMainPage()

cdef extern from "zim/file.h" namespace "zim":
    cdef cppclass File:
        File() except +
        File(string filename) except +

        Article getArticle(size_type idx)
        Article getArticle(char ns, string url)
        Article getArticleByUrl(string url)

        Fileheader getFileheader()
