import os
from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "pyzim",
    version = "0.0.1",
    author = "Matthieu Gautier",
    author_email = "mgautier@kymeria.fr",
    description = ("A wrapper around libzim."),
    license = "GPLv3",
    long_description=read('README.rst'),
    ext_modules = cythonize([
        Extension("pyzim",  ["pyzim/pyzim.pyx"],
                  libraries=["zim"],
                  language="c++")
    ])
)
