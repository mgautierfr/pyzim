=====
pyzim
=====

What is pyzim ?
---------------


Pyzim is a small python wrapper around libzim_ using cython_.

It is a minimal wrapper used to create a POC to access zim content directly in
Python.

Not all functionnalities are available. The wrapped functionnalities are :

- Open a zim file.
- Get an article of the zim file given a url.
- Get the article's content, title, url, mimetype
- Git If the article is a redirection to another article and get the
  corresponding article.

How to install ?
----------------

You will need a recent version of libzim.
The best way to get it is to compile it using kiwix-build_ ::

    $ git clone https://github.com/kiwix/kiwix-build
    $ cd kiwix-build
    $ ./kiwix-build libzim

The libzim will be installed in the `BUILD_native_dyn/INSTALL` prefix
(include in `include` directory, library in the `lib`).

Cython will need to access them when compiling the wrapper so you should set the
following environment variables ::

    $ INSTALL_PATH=<abs_path_to_kiwix_build_directory>/BUILD_native_dyn/INSTALL
    $ export CFLAGS="-I${INSTALL_PATH}/include"
    $ export LDFLAGS="-L${INSTALL_PATH}/lib64"

Then, you are ready to compile and install pyzim (in a virtual environment) ::

    $ sudo dnf install python3-Cython
    $ pew new test_zim
    $ pew toggleglobalsitepackages
    $ pip install -e .


How to use ?
------------

For now, the wrapper use `bytes`, not `str`, so you will have to pass all
filenames and urls as `bytes`.

You will need to set the `LD_LIBRARY_PATH` before running python to allow it
to found the libzim ::

    $ export LD_LIBRARY_PATH=${INSTALL_PATH/lib64
    $ python

Open a zim file ::

    import pyzim

    zim_path = "/some/where/a/zim/file.zim"
    f = pyzim.File(zim_path.encode())

Get an article ::

    article = f.get_article(b"/A/an_article.html")

    while article.is_redirect():
        article = article.get_redirect_article()

    url = article.longurl()
    title = article.title()
    mimetype = article.mimetype()
    content = article.content()

.. _libzim: http://openzim.org
.. _cython: http://cython.org
.. _kiwix-build: https://github.com/kiwix/kiwix-build
